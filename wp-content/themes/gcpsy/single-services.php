<?php
	get_header();
?>
<main>
	
	<?php
		$banner = get_field('services_banner', 'options')[0];
		$serviceContent = get_field('service')[0]['service_content'][0];
		$serviceList = get_field('service')[0]['service_checklist'];

		if($banner) {
			echo '
				<div class="secondary-banner">
					<div class="section-image banner-image" style="background-image:url('. $banner['image']['url'] .');"></div>
					<div class="banner-content">
						'. ($banner['heading'] ? '<h1>'. $banner['heading'] .'</h1>' : '') .'
						'. ($banner['content'] ? $banner['content'] : '') .'
					</div>
				</div>
			';
		}

		if($serviceContent) {
			$serviceArgs = array(
		        'post_type' => 'services',
		        'posts_per_page' => -1,
		        'orderby' => 'title',
		        'order' => 'ASC'
		    );
		    $serviceQuery = new WP_Query($serviceArgs);
		    $services = $serviceQuery->get_posts();
		    $current_id = $post->ID;

			echo '
				<div id="service-info">
					<div class="container">
						<div class="col-left">
							<div id="services-expand">More Services...</div>
							<ul id="services-list">';
							foreach($services as $service) {
								$class = 'current';

								echo '<li '. ($current_id === $service->ID ? 'class="'.$class.'"' : '') .'><a href="'. get_post_permalink($service->ID) .'">'. get_the_title($service->ID) .'</a></li>';
							}
							echo '</ul>
						</div>
						<div class="col-right">
							'.($serviceContent['heading'] ? '<h2>'. $serviceContent['heading'] .'</h2>' : '') .'
							'.($serviceContent['content'] ? $serviceContent['content'] : '') .'
						</div>
					</div>
				</div>
			';
		}

		if($serviceList) {
			$cols = array_chunk($serviceList, ceil(count($serviceList)/3)); 

			echo '
				<div id="service-list">
					<div class="container">';

						foreach($cols as $col) {
							echo '
								<div class="service-col">';
									foreach($col as $service) {
										echo '<div class="service-list-item">
												'.($service['heading'] ? '<h3>'. $service['heading'] .'</h3>' : '') .'
												'.($service['content'] ? '<p>'. $service['content'] .'</p>' : '') .'
											</div>
										';
									}
							echo '</div>
							';
						}

					echo '</div>
				</div>
			';
		}

		get_template_part( 'template-parts/cta-banner', 'page' );
	?>

</main>

<?php
	get_footer();
?>
