<?php
	// Template Name: About Page
	get_header();
?>
<main>

	<div id="about-nav">
		<div class="container">
			<?php
	            wp_nav_menu(
	                array(
	                    'container' => false,
	                    'menu' => __('About Menu'),
	                    'theme_location' => 'about-nav',
	                )
	            );
	    	?>
	    </div>
	</div>

	<?php
		$banner = get_field('banner')[0];
		$summary = get_field('summery_section');
		$qualifications = get_field('qualifications_section')[0];

		if($banner) {
			echo '
				<section class="secondary-banner">
					<div class="section-image banner-image" style="background-image:url('. $banner['image']['url'] .');"></div>';

					if($banner['heading'] || $banner['content']) {
						echo '
							<div class="banner-content">
								'. ($banner['heading'] ? '<h1>'.$banner['heading'].'</h1>' : '') .'
								'. ($banner['subheading'] ? '<h2>'. $banner['subheading'].'</h2>' : '') .'
								'. ($banner['content'] ? $banner['content']  : '') .'
								'. ($banner['button'][0]['url'] && $banner['button'][0]['text'] ? '<a class="button" href="'. $banner['button'][0]['url'].'">'.$banner['button'][0]['text'].'</a>' : '') .'
							</div>';
					}
				echo '</section>';
		}

		if($summary) {
			echo '
				<section class="general-content">
					<div class="container">
						'. $summary .'
					</div>
				</section>
			';
		}

		if($qualifications['logos']) {
			echo '
				<section id="qualifications-section">
					<div class="container">
						<div class="qualifications">';

						foreach($qualifications['logos'] as $qualification) {
							if($qualification['image']['url']) {
								echo '<div class="qualification"><img src="'. $qualification['image']['url'].'" /></div>';
							}
						}
						
				echo'	</div>
						<div class="qualifications-content">
						 '. ($qualifications['content'] ? $qualifications['content'] : '') .'
						</div>
					</div>
				</section>
			';
		}
	?>

</main>

<?php
	get_footer();
?>
