<?php
	// Template Name: Home
	get_header();
?>
<main>

	<?php

		$banner = get_field('banner');
		$pods = get_field('home_pods');
		$partners = get_field('partners');

		if($banner) {
			echo '<section class="banner">';

					foreach($banner as $slide) {

						$heading = $slide['heading'];
						$content = $slide['sub_heading'];
						$image = $slide['image'];
						$button = $slide['button'][0];

						echo '
							<div class="slide section-image" '. ($image ? 'style="background-image:url('. $image['url'] .');"' : '').'>
								<div class="container">
									'. ($heading ? '<h1>'. $heading .'</h1>' : '') .'
									'. ($content ? '<h2>'. $content .'</h2>' : '') .'
									'. ($button['url'] && $button['text'] ? '<a class="button" href="'. $button['url'] .'">'. $button['text'] .'</a>' : '') .'
								</div>
							</div>
						';
					}

			echo '</section>';
		}

		if($pods) {
			echo '<section id="home-pods">
				<div class="container">';
					foreach($pods as $pod) {
						$heading = $pod['heading'];
						$content = $pod['content'];
						$icon = $pod['icon'];
						$iconHover = $pod['icon_hover'];

						echo '<div class="home-pod">' .
							($icon ? '<img class="pod-image" src="'. $icon['url'] .'" />' : '') .
							($iconHover ? '<img class="pod-image-hover" src="'. $iconHover['url'] .'" />' : '') .
							($heading ? '<h2>'. $heading .'</h2>' : '') .
							($content ? '<p>'. $content .'</p>' : '') . 
						'</div>'
						;
					}
			echo	'</div>
			</section>';
		}

		if($partners) {
			echo '<section id="home-partners">
				<div class="container">';

			foreach($partners as $partner) {
				echo 
					($partner['partner'] ? '<img class="partner" src="'. $partner['partner']['url'] .'" />' : '')
				;
			}

			echo '</div>
			</section>';
		}

	?>

</main>

<?php
get_footer();
?>
