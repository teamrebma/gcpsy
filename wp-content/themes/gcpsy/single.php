<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		         <div class="container">
		         	<?php the_content(); ?>
		         </div>
			<?php endwhile;endif; ?>
		</main>
	</div>
</div>

<?php get_footer();
