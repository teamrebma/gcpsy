<?php

		$ctaBanner = get_field('action', 'options')[0];

		if($ctaBanner) {
			echo '
				<div class="cta-banner">
					<div class="container">
						<div class="cta-left">
							'. ($ctaBanner['content'] ? '<h2>'. $ctaBanner['content'] .'</h2>' : '') .'
						</div>
						<div class="cta-right">
							'. ($ctaBanner['call_to_action'] ? '<h3>'. $ctaBanner['call_to_action'] .'</h3>' : '') .'
							'. ($ctaBanner['button'][0]['url'] && $ctaBanner['button'][0]['text'] ? '<a class="button" href="'. $ctaBanner['button'][0]['url'] .'">'. $ctaBanner['button'][0]['text'] .'</a>' : '') .'
						</div>
					</div>
				</div>
			';
		}

?>