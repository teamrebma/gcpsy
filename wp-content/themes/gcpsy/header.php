<!doctype html>    
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php wp_title('&raquo;','true','right'); ?><?php bloginfo('name'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=yes"/>

        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/icons/favicon.png">
        <!--[if IE]>
                <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico">
        <![endif]-->        
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">	
        <?php wp_head(); ?>  
    </head>
    <body <?php body_class( 'class-name' ); ?>>
    	<header>
            <a id="logo" href="/">
                <?php
                    if(is_front_page()) {
                        echo '
                            <img src="'. get_template_directory_uri() .'/assets/icons/logo-white.png" alt="Logo" /> 

                        ';
                    } else {
                        echo '
                            <img src="'. get_template_directory_uri() .'/assets/icons/logo-grey.png" alt="Logo" /> 

                        ';
                    }
                ?>
            </a>
            <div class="container">
                <nav>
                    <?php
                        wp_nav_menu(
                            array(
                                'container' => false,
                                'menu' => __('Main Menu'),
                                'theme_location' => 'main-nav',
                            )
                        );
                    ?>                  
                </nav>
            </div>
            <div id="mobile-menu">
                <span></span>
                <span></span>
                <span></span>
            </div>
    	</header>