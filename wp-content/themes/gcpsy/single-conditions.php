<?php
	get_header();
?>
<main>
	
	<?php
		$banner = get_field('banner')[0];
		$infoSection = get_field('info_section')[0];
		$additionalInfo = get_field('additional_info')[0];

		if($banner) {
			echo '
				<section class="secondary-banner">
					<div class="section-image banner-image" style="background-image:url('. $banner['image']['url'] .');"></div>
					<div class="banner-content">
						'. ($banner['heading'] ? '<h1>'. $banner['heading'] .'</h1>' : '') .'
						'. ($banner['content'] ? $banner['content'] : '') .'
					</div>
				</section>
			';
		}

		if($infoSection) {
			echo '
				<section id="condition">
					<div class="container">
						<div class="col-left">
							'. ($infoSection['heading'] ? '<h2>About '. $infoSection['heading'] .'...</h2>' : '') .'
							<div id="condition-stats">
								'. ($infoSection['condition_percentage'] ? '<span>'. $infoSection['condition_percentage'] .'</span>' : '') .'
								'. ($infoSection['percentage_content'] ? '<p>'. $infoSection['percentage_content'] .'</p>' : '') .'
							</div>
						</div>
						<div class="col-right">
							'. ($infoSection['heading'] ? '<h2>'. $infoSection['heading'] .'</h2>' : '') .'
							'. ($infoSection['content'] ? ''. $infoSection['content'] .'' : '') .'
						</div>
					</div>
				</section>
			';
		}

		if($additionalInfo) {
			echo '
				<section id="condition-info">
					<div class="container">
						<div class="col-left">
							'. ($additionalInfo['button'][0]['url'] && $additionalInfo['button'][0]['text'] ? '<a class="button" href="'. $additionalInfo['button'][0]['url'].'">'. $additionalInfo['button'][0]['text'] .'</a>' : '') .'
						</div>
						<div class="col-right">
							'.($additionalInfo['content'] ? $additionalInfo['content'] : '') .'
						</div>
					</div>
				</section>
			';
		}

		get_template_part( 'template-parts/cta-banner', 'page' );
	?>

</main>

<?php
	get_footer();
?>
