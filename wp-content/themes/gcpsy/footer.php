<?php
	$social = get_field('footer', 'options')[0]['social_media'][0];
	$footerPod = get_field('footer', 'options')[0]['footer_pod'];
	$footerNav = get_field('footer', 'options')[0]['footer_navigation'][0];
?>

<footer>
	<div id="footer-main">
		<div class="container">
			<ul id="social">
				<?php
				echo 
					($social['facebook'] ? '<li><a href="'. $social['facebook'] .'"><i class="fa fa-facebook"></i></a></li>' : '') .
					($social['linkedin'] ? '<li><a href="'. $social['linkedin'] .'"><i class="fa fa-linkedin"></i></a></li>' : '') .
					($social['twitter'] ? '<li><a href="'. $social['facebook'] .'"><i class="fa fa-twitter"></i></a></li>' : '')
				;
				?>
			</ul>
			<?php
				foreach($footerPod as $pod) {
					echo '
						<div class="footer-pod">
							'. ($pod['heading'] ? '<h3>'. $pod['heading'] .'</h3>' : '') .'
							'. ($pod['content'] ? '<p>'. $pod['content'] .'</p>' : '') .'
						</div>
					';
				}
			?>
			<div id="footer-links">
				<?php
					echo '
						'. ($footerNav['heading'] ? '<h3>'. $footerNav['heading'] .'</h3>' : '') .'
					';

					foreach($footerNav['page_link'] as $link) {
						$pageID = $link['link']->ID;

						echo '<a href="'. get_permalink($pageID) .'">'. get_the_title($pageID) .'</a>';
					}
				?>
			</div>
		</div>
	</div>
	<div id="footer-copy">
		<div class="container">
			<p id="by-thrive">Crafted By <a href="thriveweb.com.au">Thrive</a></p>
			<p id="copy">&copy; Gold Coast Psychiatry <?php echo date('Y'); ?></p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>