<?php
	// Template Name: Contact Page
	get_header();

	$banner = get_field('banner')[0];
	$mainContent = get_field('main_content');
	$additionalInfo = get_field('additional_information');
	$contact = get_field('contact_information', 'options')[0];
	
?>
<main>

	<?php
		if($banner) {
			echo '
				<div id="contact-banner" class="section-image" '. ($banner['image']['url'] ? 'style="background-image:url('. $banner['image']['url'] .');' : '') .'">
					<div class="container">
						'.($banner['heading'] ? '<h1>'. $banner['heading'] .'</h1>' : '') .'
					</div>
				</div>
			';
		}
	?>

	<div id="contact-wrapper">
		<div class="container">
			<?php
				if($mainContent) {
					echo '
						<div id="contact-content">
							'. $mainContent .'
						</div>
					';
				}
			?>
			<div id="contact-information">
				<div id="contact-left">
					<?php

						echo '
							'. ($contact['phone_number'] ? '<a href="tel:'. $contact['phone_number'] .'" class="phone">'. $contact['phone_number'] .'</a>' : '') .'
							'. ($contact['address'] ? '<p>'. $contact['address'] .'</p>' : '') .'
							'. ($contact['hours_of_operation'] ? '<p>'. $contact['hours_of_operation'] .'</p>' : '') .'
							'. ($additionalInfo ? '<div id="additional-info">
								'. $additionalInfo .'
							</div>' : '') .'
						';

					?>
				</div>
				<div id="map"></div>

				<script type="text/javascript">
					function initMap() {
						var gcpsy = {lat: <?php echo $contact['map']['lat'] ?>, lng: <?php echo $contact['map']['lng'] ?>};
						var map = new google.maps.Map(document.getElementById('map'), {
						  zoom: 14,
						  center: gcpsy
						});
						var marker = new google.maps.Marker({
						  position: gcpsy,
						  map: map
						});
					}
				</script>
				<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9ISEjEXewZWfWX8lFAwG0h_YsZGKrO9w&callback=initMap"></script>
			</div>
		</div>
	</div>

</main>

<?php
	get_footer();
?>
