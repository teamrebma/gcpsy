<?php

class Theme {

    public static function init() {
        // Assets
        add_action('wp_print_styles', 'Theme::loadAssets');
        // Menus
        add_action('after_setup_theme', 'Theme::registerMenus');
        // Sidebars
        // add_action('widgets_init', 'Theme::registerSidebars');

        Theme::customPostTypes();

        // Option Pages
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page('Options');
        }
        return;

    }

    public static function loadAssets() {
        global $wp_styles;
        if (!is_admin()) {
            wp_enqueue_script('jquery');

            wp_register_style('font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css");
            wp_register_style('flickity-css', "https://unpkg.com/flickity@2/dist/flickity.min.css");
            wp_register_style('main-styles', get_template_directory_uri() . '/assets/css/styles.css', array(), '1.0.0');


            wp_register_script('flickity-js', "https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js");
            wp_register_script('main-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), '1.0.0',true);
         
            wp_enqueue_style('font-awesome');
            wp_enqueue_style('flickity-css');
            wp_enqueue_style('main-styles');

            wp_enqueue_script('flickity-js');
            wp_enqueue_script('main-js');

        }
        return;
    }

    public static function registerMenus() {
        // wp menus
        add_theme_support('menus');
        register_nav_menus(
                array(
                    'main-nav' => __('The Main Menu'), // nav in header                    
                    'footer-nav' => __('Footer Menu'), // nav in footer
                    'about-nav' => __('About Menu'), // nav in about page
                )
        );
        return;
    }

    public static function customPostTypes() {
        
        function custom_conditions() {
            register_post_type( 'conditions',
                array(
                    'labels' => array(
                        'name' => __( 'Conditions' ),
                        'singular_name' => __( 'Condition' )
                    ),
                    'public' => true,
                    'description' => __('This is the condition post type', 'gcpsy'),
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'conditions'),
                    'menu_icon' => 'dashicons-heart',
                    'supports' => array('title')
                )
            );
        }

        function custom_services() {
            register_post_type( 'services',
                array(
                    'labels' => array(
                        'name' => __( 'Services' ),
                        'singular_name' => __( 'Service' )
                    ),
                    'public' => true,
                    'description' => __('This is the service post type', 'gcpsy'),
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'services'),
                    'menu_icon' => 'dashicons-shield',
                    'supports' => array('title')
                )
            );
        }


        add_action( 'init', 'custom_conditions' );
        add_action( 'init', 'custom_services' );
    }
}

?>