jQuery(document).ready(function(){

	jQuery('.banner').flickity({
		prevNextButtons: false,
	});

	// Nav

		jQuery('#mobile-menu').click(function(){
			jQuery('header').toggleClass('active');
			jQuery('header .container').slideToggle('fast');
		});

		jQuery('nav ul li.menu-item-has-children').append('<i class="fa fa-plus"></i>');

		if(window.innerWidth < 860) {
			jQuery('nav ul li.menu-item-has-children').click(function(){
				jQuery(this).toggleClass('active');
				jQuery(this).find('ul').slideToggle('fast');
			});
		}

			jQuery('nav a').click(function(e){
				if(jQuery(this).attr('href') === '#') {
					e.preventDefault();
				}
			});

	// Service

		jQuery('#services-expand').click(function(){
			jQuery('#services-list').slideToggle('fast');
		});


});